
%require "3.2"
%debug
%language "c++"
%define api.token.constructor
%define api.value.type variant
%define api.location.file none
%define parse.assert
%param { zz::ast32& ast }
%locations

// *.h
%code requires {
#include "zebu4/zebu_cpp.h"

enum Token { line, comment, error };

}

// *.cc
%code {
#include <climits>  // INT_MIN, INT_MAX
#include <iostream>
#include <sstream>

namespace yy {
        // Prototype of the yylex function providing subsequent tokens.
        static parser::symbol_type yylex(zz::ast32& ast);
}; // namespace yy

}

%token<zz::ref32> BLOCK
%token<zz::ref32> KEY
%token<zz::ref32> VALUE
%token<zz::ref32> NUMBER
%token<zz::ref32> COMMENT
%token END_OF_LINE '\n';
%token END_OF_FILE 0;
%token ERROR

%type<zz::ref32> input
%type<std::vector<zz::ref32>> blocks
%type<zz::ref32> block
%type<std::vector<zz::ref32>> lines
%type<zz::ref32> line

%%

input
        : lines blocks {
                $2.insert($2.begin(), ast.push($1.begin(), $1.end()));
                zz::ref32 root = ast.push($2.begin(), $2.end());
                std::cout << zz::emit(ast, root) << '\n'; }
        ;

blocks
        : blocks block { $$ = std::move($1); $$.push_back($2); }
        | { /* empty block list */ }
        ;

block
        : BLOCK lines {
                $$ = ast.push($2.begin(), $2.end()); }
        | error {
                $$ = zz::ref32{Token::error}; }
        ;

lines
        : lines line { $$ = std::move($1); $$.push_back($2); }
        | { /* empty line list */ }
        ;

line
        : KEY VALUE {
                $$ = ast.push({zz::ref32{Token::line}, $1, $2}); }
        | COMMENT {
                $$ = ast.push({zz::ref32{Token::comment}, $1}); }
        | error {
                $$ = zz::ref32{Token::error}; }
        ;

%%

namespace yy {

static parser::symbol_type yylex(zz::ast32& ast)
{
        static unsigned line_num = 0;
        static std::string line;
        static std::string::size_type pos = 0;
        for (;;) {
                if (line[pos] == 0) {
                        if (!std::getline(std::cin, line)) {
                                parser::location_type loc{nullptr, line_num, 0};
                                return parser::make_END_OF_FILE(std::move(loc));
                        }
                        pos = 0;
                        ++line_num;
                } else if (isspace(line[pos])) {
                        ++pos;
                } else {
                        break;
                }
        }

        parser::location_type loc{nullptr, line_num, (unsigned)pos};
        if (line[pos] == '=') {
                auto first = line.find_first_not_of(" \t\r\n", pos + 1);
                auto last = line.find_last_not_of(" \t\r\n", line.size());
                std::string_view v(&line[first], last - first + 1);
                pos = line.size();
                return parser::make_VALUE(ast.push(v), std::move(loc));
        } else if (line[pos] == '[') {
                ++pos;
                auto end = line.find(']', pos);
                if (end == std::string::npos) return parser::make_ERROR(std::move(loc));
                std::string_view v(&line[pos], end - pos);
                pos = end + 1;
                return parser::make_BLOCK(ast.push(v), std::move(loc));
        } else if (line[pos] == '#') {
                std::string_view v(&line[++pos]);
                pos += v.size();
                return parser::make_COMMENT(ast.push(v), std::move(loc));
        } else {
                auto end = line.find_first_of(" \t\r\n=", pos);
                if (end == std::string::npos) end = line.size();
                std::string_view v(&line[pos], end - pos);
                pos = end;
                return parser::make_KEY(ast.push(v), std::move(loc));
        }
        return parser::make_ERROR(std::move(loc));
}

void parser::error (const parser::location_type& loc, const std::string& msg)
{ std::cerr << loc << ": " << msg << '\n'; }

}; // namespace yy

int main(int argc, const char *argv[])
{
        zz::ast32 ast;
        auto&& p = yy::parser{ast};
        p.set_debug_level (!!getenv ("YYDEBUG"));
        return p.parse ();
}

