%code requires {
#include "zebu4/zebu_cpp.h"
}

%require "3.2"
%debug
%language "c++"
%define api.token.constructor
%define api.value.type variant
%define api.location.file none
%define parse.assert
%param { zz::ast32& ast }
%locations

%code {
#include <climits>  // INT_MIN, INT_MAX
#include <iostream>
#include <sstream>

namespace yy {
        // Prototype of the yylex function providing subsequent tokens.
        static parser::symbol_type yylex(zz::ast32& ast);
}

}

%token <zz::ref32> TEXT;
%token <zz::ref32> NUMBER;
%token END_OF_FILE 0;

%type <zz::ref32> item;
%type <std::vector<zz::ref32>> list;

%%

result
        : list  {
                zz::Ref root = ast.push($1.begin(), $1.end());
                std::cout << zz::emit(ast, root) << '\n'; }
        ;

list
        : %empty     { /* Generates an empty zz::Buf */ }
        | list item  { $$ = std::move($1); $$.push_back($2); }
        ;

item
        : TEXT
        | NUMBER
        ;
%%

namespace yy {

static parser::symbol_type yylex(zz::ast32& ast)
{
        while (isspace(std::cin.peek())) std::cin.get();
        parser::location_type loc{nullptr, 0, 0};
        if (isdigit(std::cin.peek())) {
                int i;
                std::cin >> i;
                return parser::make_NUMBER(ast.push(i), std::move(loc));
        } else if (isalpha(std::cin.peek())) {
                std::string s;
                std::cin >> s;
                return parser::make_TEXT(ast.push(s), std::move(loc));
        } else {
                return parser::make_END_OF_FILE(std::move(loc));
        }
}

void parser::error (const parser::location_type& loc, const std::string& msg)
{ std::cerr << loc << ": " << msg << '\n'; }

}; // end namespace yy

int main(int argc, const char *argv[])
{
        zz::ast32 ast;
        auto&& p = yy::parser{ast};
        p.set_debug_level (!!getenv ("YYDEBUG"));
        return p.parse ();
}

