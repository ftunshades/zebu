%code requires{
#include "zebu4/zebu.h"
enum Token { TOK_ADD, TOK_SUB, TOK_MUL, TOK_DIV, TOK_NEG, TOK_ERROR };
}

%code provides{
int yylex(YYSTYPE* lvalp, YYLTYPE* llocp, struct zz_ast* ast);
void yyerror(YYLTYPE* llocp, struct zz_ast* ast, const char *msg);
}

%define api.pure full
%locations
%param {struct zz_ast* ast}
%define api.value.type {size_t}
%define parse.error verbose

%token '+'
%token '-'
%token '*'
%token '/'
%token 'n'
%token NUM

%%

input
        : exp { zz_print(ast, $1, stdout); }
        ;

exp
        : NUM
        | exp exp '+' { $$ = zz_push_list(ast, TOK_ADD, $1, $2); }
        | exp exp '-' { $$ = zz_push_list(ast, TOK_SUB, $1, $2); }
        | exp exp '*' { $$ = zz_push_list(ast, TOK_MUL, $1, $2); }
        | exp exp '/' { $$ = zz_push_list(ast, TOK_DIV, $1, $2); }
        | exp 'n' { $$ = zz_push_list(ast, TOK_NEG, $1); }
        | error { $$ = TOK_ERROR; }
        ;

%%

#include <ctype.h>

int yylex(YYSTYPE* lvalp, YYLTYPE* llocp, struct zz_ast* ast)
{
        static long line_off = 0;
        static long line_num = 1;
        char c;
        for (c = fgetc(stdin); isspace(c); c = fgetc(stdin)) {
                if (c == '\n') {
                        ++line_num;
                        line_off = ftell(stdin);
                }
        }
        unsigned col_num = ftell(stdin) - line_off + 1;
        llocp->first_line = line_num;
        llocp->first_column = col_num;
        if (isdigit(c)) {
                ungetc(c, stdin);
                int n;
                fscanf(stdin, "%d", &n);
                *lvalp = zz_push_integer(ast, n);
                return NUM;
        }
        return c;
}

void yyerror(YYLTYPE* llocp, struct zz_ast* ast, const char *msg)
{
        fprintf(stderr, "%d,%d:%s\n", llocp->first_line, llocp->first_column, msg);
}

int main(int argc, const char *argv[])
{
        struct zz_ast ast = zz_ast();
        return yyparse(&ast);
}

