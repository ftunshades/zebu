#ifndef ZZ_H_
#define ZZ_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

#define ZZ_VERSION "4.1.0"
#define ZZ_VERSION_MAJOR 4
#define ZZ_VERSION_MINOR 1
#define ZZ_VERSION_PATCH 0

#ifndef ZZ_BASE
#define ZZ_BASE uint32_t
#endif

#define ZZ_TINYINT      0
#define ZZ_INTEGER      ((ZZ_BASE)1 << (sizeof(ZZ_BASE) * 8 - 4))
#define ZZ_REAL         ((ZZ_BASE)2 << (sizeof(ZZ_BASE) * 8 - 4))
#define ZZ_STRING       ((ZZ_BASE)3 << (sizeof(ZZ_BASE) * 8 - 4))
#define ZZ_LIST         ((ZZ_BASE)4 << (sizeof(ZZ_BASE) * 8 - 4))
#define ZZ_MASK         (ZZ_TINYINT | ZZ_INTEGER | ZZ_REAL | ZZ_STRING | ZZ_LIST)

#define ZZ_NPOS         ((ZZ_BASE)-1)

struct zz_ast {
        ZZ_BASE *data;
        ZZ_BASE size;
        ZZ_BASE capacity;
};

struct zz_list {
        ZZ_BASE size;
        ZZ_BASE data[];
};

struct zz_buf {
        ZZ_BASE size;
        ZZ_BASE capacity;
        ZZ_BASE data[];
};

struct zz_buf *zz_append(struct zz_buf *buf, ZZ_BASE value)
{
        if (buf == NULL) {
                buf = malloc(sizeof(*buf) + 2 * sizeof(value));
                buf->size = 0;
                buf->capacity = 2;
        } else if (buf->size == buf->capacity) {
                buf->capacity *= 2;
                buf = realloc(buf, sizeof(*buf) + buf->capacity * sizeof(value));
        }
        buf->data[buf->size++] = value;
        return buf;
}

struct zz_buf *zz_prepend(struct zz_buf *buf, ZZ_BASE value)
{
        buf = zz_append(buf, 0);
        memmove(buf->data + 1, buf->data, (buf->size - 1) * sizeof(ZZ_BASE));
        buf->data[0] = value;
        return buf;
}

struct zz_ast zz_ast(void) { return (struct zz_ast){NULL, 0, 0}; }

void zz_destroy(struct zz_ast* ast)
{ free(ast->data); }

ZZ_BASE zz_push(struct zz_ast* ast, const ZZ_BASE* buf, ZZ_BASE len)
{
        ZZ_BASE ret = ast->size;
        ZZ_BASE new_size = ast->size + len;
        if (ast->capacity < new_size) {
                if (ast->capacity == 0) ast->capacity = 2;
                while (ast->capacity < new_size) ast->capacity *= 2;
                ast->data = realloc(ast->data, ast->capacity * sizeof(*ast->data));
        }
        memcpy(ast->data + ast->size, buf, len * sizeof(*ast->data));
        ast->size = new_size;
        return ret;
}

ZZ_BASE zz_type(ZZ_BASE pos) { return pos & ZZ_MASK; }
ZZ_BASE zz_index(ZZ_BASE pos) { return pos & ~ZZ_MASK; }

ZZ_BASE zz_push_integer(struct zz_ast* ast, long value)
{
        int len = (sizeof(long) - 1) / sizeof(ZZ_BASE) + 1;
        return zz_push(ast, (void *)&value, len) | ZZ_INTEGER;
}

ZZ_BASE zz_push_real(struct zz_ast* ast, double value)
{
        int len = (sizeof(double) - 1) / sizeof(ZZ_BASE) + 1;
        return zz_push(ast, (void *)&value, len) | ZZ_REAL;
}

ZZ_BASE zz_push_string(struct zz_ast* ast, const char *str, ZZ_BASE len)
{
        if (len < 0) len = strlen(str);
        ZZ_BASE pos = zz_push(ast, (void *)str, len / sizeof(ZZ_BASE) + 1);
        ((char *)&ast->data[pos])[len] = 0;
        return pos | ZZ_STRING;
}

ZZ_BASE zz_push_list_impl(struct zz_ast* ast, ZZ_BASE *buf, ZZ_BASE len)
{
        ZZ_BASE pos = zz_push(ast, &len, 1);
        zz_push(ast, buf, len);
        return pos | ZZ_LIST;
}

#define zz_push_list(ast, ...) \
        zz_push_list_impl(ast, (ZZ_BASE[]){__VA_ARGS__},\
                        (sizeof((ZZ_BASE[]){__VA_ARGS__})/sizeof(ZZ_BASE)))

ZZ_BASE zz_push_buf(struct zz_ast *ast, struct zz_buf *buf)
{
        static const ZZ_BASE zero = 0;
        if (buf == NULL) return zz_push(ast, &zero, 1) | ZZ_LIST;
        ZZ_BASE pos = zz_push(ast, &buf->size, 1);
        zz_push(ast, buf->data, buf->size);
        free(buf);
        return pos | ZZ_LIST;
}

#define zz_get(ast, pos) ((ast)->data[zz_index(pos)])

int zz_get_integer(struct zz_ast* ast, ZZ_BASE pos)
{
        if (zz_type(pos) == ZZ_TINYINT) return zz_index(pos);
        if (zz_type(pos) == ZZ_INTEGER) return zz_get(ast, pos);
        abort();
}

double zz_get_real(struct zz_ast *ast, ZZ_BASE pos)
{
        if (zz_type(pos) == ZZ_REAL) return *(double *)&zz_get(ast, pos);
        abort();
}

const char* zz_get_string(struct zz_ast* ast, ZZ_BASE pos)
{
        if (zz_type(pos) == ZZ_STRING) return (char*)&zz_get(ast, pos);
        abort();
}

struct zz_list* zz_get_list(struct zz_ast* ast, ZZ_BASE pos)
{
        if (zz_type(pos) == ZZ_LIST) return (struct zz_list*)&zz_get(ast, pos);
        abort();
}

void zz_print(struct zz_ast* ast, ZZ_BASE pos, FILE *out)
{
        ZZ_BASE type = zz_type(pos);
        if (pos == ZZ_NPOS) {
                fprintf(out, "()");
        } else if (type == ZZ_TINYINT || type == ZZ_INTEGER) {
                fprintf(out, "%d", zz_get_integer(ast, pos));
        } else if (type == ZZ_REAL) {
                fprintf(out, "%g", zz_get_real(ast, pos));
        } else if (type == ZZ_STRING) {
                fprintf(out, "\"%s\"", zz_get_string(ast, pos));
        } else if (type == ZZ_LIST) {
                fputc('(', out);
                struct zz_list* list = zz_get_list(ast, pos);
                if (list->size > 0) {
                        zz_print(ast, list->data[0], out);
                        for (int i = 1; i != list->size; ++i) {
                                fputc(' ', out);
                                zz_print(ast, list->data[i], out);
                        }
                }
                fputc(')', out);
        }
}

void zz_println(struct zz_ast *ast, ZZ_BASE pos, FILE *out)
{
        zz_print(ast, pos, out);
        fputc('\n', out);
}

#define zz_foreach(iter, list) \
        for (ZZ_BASE _i = 0; iter = (*(list))[_i], _i != (list)->size; ++_i)

#ifdef __cplusplus
}
#endif

#endif // ZZ_H_


